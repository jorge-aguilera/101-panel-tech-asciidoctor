package restexcel

import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook

class SheetController {

    def index() {
        Workbook workbook = new XSSFWorkbook(new File('excel/bbdd.xlsx')); //<1>
        def ret = []
        workbook.sheetIterator().each { //<2>
            ret << [name: it.sheetName, lastRowNum: it.lastRowNum, firstRowNum: it.firstRowNum]
        }
        respond ret, model:[sheetsCount:ret.size()] //<3>
    }

}
