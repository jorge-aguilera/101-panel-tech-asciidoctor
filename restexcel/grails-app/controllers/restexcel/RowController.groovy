package restexcel

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook

class RowController {
	static responseFormats = ['json', 'xml']

    def show(String id) {
        Workbook workbook = new XSSFWorkbook(new File('excel/bbdd.xlsx'));
        def ret = []
        println id
        println workbook
        Sheet sheet = workbook.getSheet(id)
        if(!sheet){
            respond 404
        }

        sheet.rowIterator().eachWithIndex{ row, idx ->
            def cols = []
            row.cellIterator().eachWithIndex{ Cell entry, int i ->
                cols << [col: i, value: "$entry"]
            }
            ret << [ idx: idx, values: cols]
        }
        respond ret, model:[rowsCount:ret.size()]
    }


}
