[[overview-http-status-codes]]
=== HTTP status codes

|===
| Status code | Usage

| 200 OK
| The request completed successfully

| 201 Created
| A new resource has been created successfully. The resource's URI is available from the response's
`Location` header

| 204 No Content
| An update to an existing resource has been applied successfully

| 400 Bad Request
| The request was malformed. The response body will include an error providing further information

| 404 Not Found
| The requested resource did not exist

| 405 Method Not Allowed
| The type of request for this resource is not allowed. For example, some endpoints may be GET only.
Trying a POST will return this message.

| 422 Unprocessable Entity
| There was an error in the request. The response body will contain the error message.
|===