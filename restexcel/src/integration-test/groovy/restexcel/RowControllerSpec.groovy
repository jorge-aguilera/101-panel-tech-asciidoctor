package restexcel

import grails.testing.mixin.integration.Integration
import grails.transaction.Rollback
import groovy.transform.CompileStatic
import io.restassured.builder.RequestSpecBuilder
import io.restassured.specification.RequestSpecification
import org.springframework.beans.factory.annotation.Value
import org.springframework.restdocs.ManualRestDocumentation
import org.springframework.restdocs.payload.FieldDescriptor
import org.springframework.restdocs.payload.JsonFieldType


import static io.restassured.RestAssured.given
import static org.hamcrest.CoreMatchers.is
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.restassured3.operation.preprocess.RestAssuredPreprocessors.modifyUris
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.documentationConfiguration

import org.springframework.http.MediaType
import spock.lang.Specification

class RowControllerSpec extends BaseSpec {

    List<FieldDescriptor>rowFields=[
            fieldWithPath('[].idx').description('Indice de la fila'),
            fieldWithPath('[].values').description('Un array de los valores'),
            fieldWithPath('[].values[].col').description('El indice de la columna'),
            fieldWithPath('[].values[].value').description('El valor de la columna'),
    ]

    void "test list rows of sheet"(String sheet, int row) {
        expect:
        buildRequestSpecification('list-row', responseFields(rowFields))
                .when()
                .get("/row/$sheet")
                .then()
                .assertThat()
                .statusCode(is(200))
        where:
        sheet | row
        "Hoja1" | 3

    }
}
