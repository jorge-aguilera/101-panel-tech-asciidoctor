package restexcel

import grails.testing.mixin.integration.Integration
import grails.transaction.Rollback
import groovy.transform.CompileStatic
import io.restassured.builder.RequestSpecBuilder
import io.restassured.specification.RequestSpecification
import org.springframework.beans.factory.annotation.Value
import org.springframework.restdocs.ManualRestDocumentation
import org.springframework.restdocs.payload.FieldDescriptor
import org.springframework.restdocs.payload.JsonFieldType
import org.springframework.restdocs.snippet.Snippet

import static io.restassured.RestAssured.given
import static org.hamcrest.CoreMatchers.is
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.restassured3.operation.preprocess.RestAssuredPreprocessors.modifyUris
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.documentationConfiguration

import org.springframework.http.MediaType
import spock.lang.Specification

@Integration
@Rollback
class BaseSpec extends Specification {

    final ManualRestDocumentation restDocumentation = new ManualRestDocumentation('build/generated-snippets')

    @Value('${local.server.port}')
    Integer serverPort

    protected RequestSpecification documentationSpec

    def setup() {
        documentationSpec = new RequestSpecBuilder()
                .addFilter(documentationConfiguration(restDocumentation))
                .build()
        restDocumentation.beforeTest(getClass(), specificationContext.currentSpec.name)
    }

    def cleanup() {
    }

    RequestSpecification buildRequestSpecification(String identifier, Snippet... snippets ) {
        given(documentationSpec)
                .accept(MediaType.APPLICATION_JSON.toString())
                .filter(
                document(identifier,
                        preprocessRequest(prettyPrint(), modifyUris().scheme("https").host("restexcel.api.com").removePort()),
                        preprocessResponse(prettyPrint()),
                        snippets
                )
        )
                .port(this.serverPort)
    }
}
