import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.*
import groovy.servlet.*

def startJetty() {
    def server = new Server(args ? args[0] as int : 8081)

    def handler = new ServletContextHandler(ServletContextHandler.NO_SESSIONS)
    handler.contextPath = args && args.length > 1 ? args[1] : '/'
    handler.resourceBase = '.'
    def servlet = handler.addServlet(DefaultServlet, '/')
    servlet.setInitParameter('resourceBase', './build/asciidoc')
    servlet.setInitParameter('useFileMappedBuffer', 'false')

    server.handler = handler
    server.start()
}

println "Starting Jetty, press Ctrl+C to stop."
startJetty()
